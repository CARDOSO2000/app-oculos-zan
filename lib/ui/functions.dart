import 'package:flutter/material.dart';

class FunctionPage extends StatefulWidget {

  const FunctionPage({Key? key}) : super(key: key);

  @override
  State<FunctionPage> createState() => _FunctionsPage();
}

class _FunctionsPage extends State<FunctionPage>{
  bool text = false;
  @override
  Widget build (BuildContext context){
    return Scaffold(
      appBar: AppBar(
        // Overide the default Back button
          title: Text('Funções'),
          automaticallyImplyLeading: false,
          leadingWidth: 100,
          leading: ElevatedButton.icon(
            onPressed: () => Navigator.of(context).pop(),
            icon: const Icon(Icons.arrow_left_sharp),
            label: const Text('Back'),
            style: ElevatedButton.styleFrom(
                elevation: 0, primary: Colors.transparent),
          )),
      body: Center(
        child: SwitchListTile(
          title: const Text('Text Mode?'),
          value: text,
          onChanged: (bool value) {
            setState(() {
              text = value;
            });
          },
          secondary: const Icon(Icons.announcement_rounded),
        ),
      ),
    );
  }
}