import 'package:flutter/material.dart';

class ConfigPage extends StatefulWidget {

  const ConfigPage({Key? key}) : super(key: key);

  @override
  State<ConfigPage> createState() => _ConfigPage();
}

class _ConfigPage extends State<ConfigPage>{
  bool text = false;
  double value = 0;
  @override
  Widget build (BuildContext context){
    return Scaffold(
      appBar: AppBar(
        // Overide the default Back button
          title: Text('Configurações'),
          automaticallyImplyLeading: false,
          leadingWidth: 100,
          leading: ElevatedButton.icon(
            onPressed: () => Navigator.of(context).pop(),
            icon: const Icon(Icons.arrow_left_sharp),
            label: const Text('Back'),
            style: ElevatedButton.styleFrom(
                elevation: 0, primary: Colors.transparent),
          )),
      body: Center(

        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [ SwitchListTile(
            title: const Text('Image and text in black and white?'),
            value: text,
            onChanged: (bool value) {
              setState(() {
                text = value;
              });
            },
            secondary: const Icon(Icons.announcement_rounded),
          ),
            Text("Current contrast value: $value"),
            Slider(
              value: value,
              onChanged: (newvalue){
                value = newvalue;
                setState(() {

                });
              },
              min: 0, //
              max:  1,
              divisions: 100,
            )],
        )
      ),
    );
  }
}