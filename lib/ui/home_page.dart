import 'package:app_oculos_zan/ui/configurations.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'functions.dart';

class HomePage extends StatelessWidget{
  @override
  Widget build (BuildContext context){
    return Scaffold(
        appBar: AppBar(
          // Overide the default Back button
          title: Text('Status Oculos: desconectado'),
            automaticallyImplyLeading: false,
            leadingWidth: 100,
            leading: ElevatedButton.icon(
              onPressed: () => FirebaseAuth.instance.signOut(),
              icon: const Icon(Icons.arrow_left_sharp),
              label: const Text('Back'),
              style: ElevatedButton.styleFrom(
                  elevation: 0, primary: Colors.transparent),
            )),
        body: Center(
          child: ElevatedButton(
            child: const Text('Funcionalidades'),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => FunctionPage()),
              );
            },
          )),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ConfigPage()),
            );
          },
          tooltip: 'Increment',
          child: const Icon(Icons.app_settings_alt),
        )
    );
  }
}